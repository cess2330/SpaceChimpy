//
//  GameOverScene.swift
//  SpaceChimpy
//
//  Created by Cesar Castillo on 10/03/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import Foundation
import SpriteKit

class GameOverScene: SKScene {
    
    let hasWon : Bool
    let gameOverWin = SKSpriteNode(imageNamed: "youwinbutton")
    let gameOver = SKSpriteNode(imageNamed: "gameoverbutton")
    
    
    init(size: CGSize, hasWon: Bool) {
        self.hasWon = hasWon
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Init Coder no ha sido implementado")
    }
    
    
    override func didMove(to view: SKView) {
        
        var background :SKSpriteNode
        
        if (hasWon) {
            
            gameOverWin.position = CGPoint(x: size.width/2, y: size.height/2)
            gameOverWin.zPosition = 1
            addChild(gameOverWin)
            background = SKSpriteNode(imageNamed: "estrellado")
            //run(SKAction.sequence([SKAction.wait(forDuration: 0.25), SKAction.playSoundFileNamed("win", waitForCompletion: false)]))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(Win),SKAction.wait(forDuration: 1.0)])))
            
        } else {
            
            gameOver.position = CGPoint(x: size.width/2, y: size.height+1)
            gameOver.zPosition = 1
            addChild(gameOver)
            background = SKSpriteNode(imageNamed: "estrellado")
//            run(SKAction.sequence([SKAction.wait(forDuration: 0.25), SKAction.playSoundFileNamed("lose", waitForCompletion: false)]))
            let down = SKAction.moveBy(x: 0.0, y: -size.height/2 , duration: 2.0)
            gameOver.run(down)
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(GameOver),SKAction.wait(forDuration: 1.0)])))
            
        }
        
        background.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        addChild(background)
        
//        let waitAction = SKAction.wait(forDuration: 4.0)
//        let blockAction = SKAction.run { 
//            let gameMainMenu = MainMenuScene(size: self.size)
//            gameMainMenu.scaleMode = self.scaleMode
//            let transition = SKTransition.doorsCloseHorizontal(withDuration: 1.0)
//            self.view?.presentScene(gameMainMenu, transition: transition)
//            
//        }
//        
//        self.run(SKAction.sequence([waitAction,blockAction]))
     }
    
    func GameOver() {
        
            //let moveDown = SKAction.moveBy(x: 0.0, y: -150, duration: 5.0)
        let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
        let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
        gameOver.run(SKAction.sequence([moveUp,moveDown]))
            
        
    }
    
    func Win() {
        
        
        let scaleUP = SKAction.scale(by: 1.25, duration: 0.5)
        let scaleDown = scaleUP.reversed()
        let fullScale = SKAction.sequence([scaleUP,scaleDown])
        gameOverWin.run(fullScale)

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let gameMainMenu = MainMenuScene(size: self.size)
        gameMainMenu.scaleMode = self.scaleMode
        let transition = SKTransition.doorsCloseHorizontal(withDuration: 1.0)
        self.view?.presentScene(gameMainMenu, transition: transition)
    }
    
    

    
    
    
    
    
    
    
    
}
