//
//  GameViewController.swift
//  SpaceChimpy
//
//  Created by Cesar Castillo on 17/03/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import GameKit
import iAd

class GameViewController: UIViewController, GKGameCenterControllerDelegate,ADBannerViewDelegate {

    var score : Int?
    var hightScore : Int?
    
    //var screenHeight = UIScreen.main.bounds.height
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authPlayer()
        //saveHighscore(number: (hightScore)!)
        
        
        
        if let view = self.view as! SKView? {
            // Load the SKScene from 'GameScene.sks'
            let scene = MainMenuScene(size: CGSize(width: 2048, height: 1536))
            
            // Set the scale mode to scale to fit the window
            scene.scaleMode = .aspectFill
            
            // Present the scene
            
            view.presentScene(scene)
            
            view.ignoresSiblingOrder = true
            
            view.showsFPS = false
            view.showsNodeCount = false
        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    func authPlayer() {
        let localPlayer = GKLocalPlayer.localPlayer()
        
        localPlayer.authenticateHandler = {
        (view, error) in
            
            if view != nil {
                self.present(view!, animated: true, completion: nil)
            } else {
                print(GKLocalPlayer.localPlayer().isAuthenticated)
            }
            
        }
    }
    
    func saveHighscore(number:Int) {
        if GKLocalPlayer.localPlayer().isAuthenticated {
            let scoreReporter = GKScore(leaderboardIdentifier: "SpaceChimpy")
            scoreReporter.value = Int64(number)
            let scoreArray : [GKScore] = [scoreReporter]
            
            GKScore.report(scoreArray, withCompletionHandler: nil)
        }
        
    }
    
    func showLeaderBoard() {
        let VC = self.view.window?.rootViewController
        let gvc = GKGameCenterViewController()
        
        gvc.gameCenterDelegate = self
        
        VC?.present(gvc, animated: true, completion: nil)
    }
    
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
    
}
