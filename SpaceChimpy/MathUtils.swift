//
//  MathUtils.swift
//  SpaceChimpy
//
//  Created by Cesar Castillo on 01/03/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import Foundation
import CoreGraphics

//Función que dado dos puntos devuelve uno nuevo con la suma de los dos anteiores

func + (firstPoint: CGPoint, secondPoint: CGPoint) -> CGPoint {
    
    return CGPoint(x:firstPoint.x + secondPoint.x, y:firstPoint.y + secondPoint.y)
    
}

//Función que dado dos puntos le suma al primero el segundo y lo devuelve como parámetro
func += ( firstPoint: inout CGPoint, secondPoint: CGPoint) {
    firstPoint = firstPoint + secondPoint
}

//Función que dado dos puntos devuelve uno nuevo con la resta de los dos anteiores

func - (firstPoint: CGPoint, secondPoint: CGPoint) -> CGPoint {
    
    return CGPoint(x:firstPoint.x - secondPoint.x, y:firstPoint.y - secondPoint.y)
    
}

//Función que dado dos puntos le resta al primero el segundo y lo devuelve como parámetro
func -= ( firstPoint: inout CGPoint, secondPoint: CGPoint) {
    firstPoint = firstPoint - secondPoint
}

//Función que dado dos puntos devuelve uno nuevo con la multiplicación de los dos anteiores

func * (firstPoint: CGPoint, secondPoint: CGPoint) -> CGPoint {
    
    return CGPoint(x:firstPoint.x * secondPoint.x, y:firstPoint.y * secondPoint.y)
    
}

//Función que dado dos puntos le multiplica al primero el segundo y lo devuelve como parámetro
func *= ( firstPoint: inout CGPoint, secondPoint: CGPoint) {
    firstPoint = firstPoint * secondPoint
}

//////////////////


//Función que dado un punto y un escalar multiplicada cada coordenada del punto  por el escalar

func * (point: CGPoint, scalar: CGFloat) -> CGPoint {
    
    return CGPoint(x:point.x * scalar, y:point.y * scalar)
    
}

//Función que dado un punto y un escalar los multiplica y devuelve como parámetro
func *= ( point: inout CGPoint, scarlar: CGFloat) {
    point = point * scarlar
}

//////////////////////////



//Función que dado dos puntos devuelve uno nuevo con la división de los dos anteriores
func / (firstPoint: CGPoint, secondPoint: CGPoint) -> CGPoint {
    
    return CGPoint(x:firstPoint.x / secondPoint.x, y:firstPoint.y / secondPoint.y)
    
}

//Función que dado dos puntos le divide al primero el segundo y lo devuelve como parámetro
func /= ( firstPoint: inout CGPoint, secondPoint: CGPoint) {
    firstPoint = firstPoint / secondPoint
}


//Función que dado un punto y un escalar divide cada coordenada del punto  por el escalar

func / (point: CGPoint, scalar: CGFloat) -> CGPoint {
    
    return CGPoint(x:point.x / scalar, y:point.y / scalar)
    
}

//Función que dado un punto y un escalar los divide y devuelve como parámetro
func /= ( point: inout CGPoint, scalar: CGFloat) {
    point = point / scalar
}

//Solo se ejecutará si la app corre en 32 bits
#if !(arch(x86_64) || arch(arm64))
    
    func atan2(x:CGFloat, y:CGFloat) -> CGFloat {
        return CGFloat(atan2f(Float(x), Float(y)))
    }

    func sqrt(a:CGFloat) -> CGFloat {
        return CGFloat(sqrtf(Float(a)))
    }
#endif

extension CGPoint {
    //Calcula la longitud del punto utilizando el teorema de pitágoras
    func length() -> CGFloat {
        return sqrt(x*x+y*y)
    }
    //Normaliza el vactor para que tenga longitud 1
    func normalize() -> CGPoint {
        return self / length()
    }
    // Encuentra el ángulo que forma el punto con la horizontal
    func angle() -> CGFloat {
        return atan2(y, x)
    }
}

let π = CGFloat(M_PI)

//Gira el sprite de modo que tenga que rotar siempre lo menos posible
func shortestAngleBetween(angle1:CGFloat, angle2: CGFloat) -> CGFloat {
    let twoπ : CGFloat = 2.0 * π
    
    var angle = (angle2 - angle1).truncatingRemainder(dividingBy: twoπ) // Los resultados siempre estaran entre 0 y 2π
    if angle >= π {
        angle -= twoπ
    }
    if angle <= -π {
        angle += twoπ
    }
    
    //Después de este fragmento, seguro que el ángulo se encuantra entre  - π y π, por lo tanto sabra hacia donde totar con menor diferencia respecto a su posición original
    return angle
}

//Devuelve la distancia en pixels entre los puntos p1 y p2
func distanceBetweenPoints(p1:CGPoint,p2:CGPoint) -> CGFloat {
    let distanceVector = p2-p1
    let distance = distanceVector.length()
    
    return distance
    
}


extension CGFloat {
    func sign() -> CGFloat {
        return (self >= 0) ? 1.0 : -1.0
    }
}
