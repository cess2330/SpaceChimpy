//
//  MainMenuScene.swift
//  SpaceChimpy
//
//  Created by Cesar Castillo on 10/03/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import Foundation
import SpriteKit


class MainMenuScene: SKScene {
    
    var chimp = SKSpriteNode(imageNamed: "chimp1")
    let chimpAnimation : SKAction
    var roca = SKSpriteNode(imageNamed: "enemy")
    var nave = SKSpriteNode(imageNamed: "enemynave")
    let chimpAnglesPerSecond : CGFloat = 4.0 * π
    var velocity = CGPoint(x: 300, y: 300)
    var dt : TimeInterval = 0
    let playButton = SKSpriteNode(imageNamed: "playbutton")
    let logo = SKSpriteNode(imageNamed: "Logo")
    let central = SKSpriteNode(imageNamed: "central")

   
    
    override init (size: CGSize) {
        
        var chimpTextures : [SKTexture] = []
        
        for i in 1...4 {
            chimpTextures.append(SKTexture(imageNamed: "chimp\(i)"))
        }
        
        chimpTextures.append(chimpTextures[2])
        chimpTextures.append(chimpTextures[1])
        chimpAnimation = SKAction.repeatForever(SKAction.animate(with: chimpTextures, timePerFrame: 0.15))
        
   

        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("El metodo init no ha sido implementado")
    }
    
    override func didMove(to view: SKView) {
        let background = SKSpriteNode(imageNamed: "MainMenu")
        background.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        background.zPosition = -1
        addChild(background)
        
        
        logo.position = CGPoint(x: size.width/2, y: size.height/2+250)
        logo.zPosition = 2
        logo.setScale(0)
        addChild(logo)
       
      
       animateLogo()
        
        chimp.position = CGPoint(x: 500, y: 550)
        chimp.zPosition = 3
        chimp.xScale = 1.5
        chimp.yScale = 1.5
        addChild(chimp)
        
    
        
        nave.position = CGPoint(x: 1600, y: 550)
        nave.zPosition = 1
        addChild(nave)
        
        playButton.name = "playbutton"
        playButton.position = CGPoint(x: size.width/2, y: (size.height/2)-playButton.size.height)
        playButton.zPosition = 2
        playButton.xScale = 1.5
        playButton.yScale = 1.5
        addChild(playButton)
        
        
        
        //let down = SKAction.moveBy(x: 0.0, y: -size.height/2 + 260, duration: 2.0)
        //central.run(down)
        
         let scaleUP = SKAction.scale(to: 1.0, duration: 0.9)
            logo.run(scaleUP)
        
    run(SKAction.repeatForever(SKAction.sequence([SKAction.run(logoAppear),SKAction.wait(forDuration: 16.2)])))
      run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animateLogo),SKAction.wait(forDuration: 1.0)])))
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animateLogo),SKAction.wait(forDuration: 2.0)])))
        
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animateNave),SKAction.wait(forDuration: 2.0)])))
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animateChimpy),SKAction.wait(forDuration: 2.0)])))
        
        
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(animatePlayButton),SKAction.wait(forDuration: 1.6)])))
        
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemy),SKAction.wait(forDuration: 1.5)])))
        run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyDistinto),SKAction.wait(forDuration: 2.0)])))
        
        playBackgroundMusic(filename: "mainmenumusic.mp3")
        
    }
    func animateChimp() {
        if chimp.action(forKey: "animation") == nil {
            chimp.run(SKAction.repeatForever(chimpAnimation), withKey: "animation")
        }
        
    }
    
    func animatePlayButton() {
        
        let scaleUP = SKAction.scale(by: 1.25, duration: 0.8)
        let scaleDown = scaleUP.reversed()
        let fullScale = SKAction.sequence([scaleUP,scaleDown])
        playButton.run(fullScale)
    }
    
    func logoAppear() {
        
        let scaleUP = SKAction.scale(by: 1.25, duration: 0.5)
        let scaleDown = scaleUP.reversed()
        let fullScale = SKAction.sequence([scaleUP,scaleDown])
        logo.run(fullScale)
        
    }
    func animateLogo() {
       
        
        //let moveDown = SKAction.moveBy(x: 0.0, y: -150, duration: 5.0)
        let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
        let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
        logo.run(SKAction.sequence([moveUp,moveDown]))
        
    }
    
    func animateNave() {
        
       
        let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
        let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
        nave.run(SKAction.sequence([moveUp,moveDown]))
        
    }
    
    func animateChimpy() {
        
        
        let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
        let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
        nave.run(SKAction.sequence([moveUp,moveDown]))
        
    }
    
    func spawnEnemy() {
        let enemy = SKSpriteNode(imageNamed: "enemy.png")
        enemy.name = "enemy"
        enemy.zPosition = 1
        enemy.zRotation = CGFloat.random(min: enemy.size.height - enemy.size.height, max: enemy.size.height)
        enemy.position = CGPoint(x: size.width + enemy.size.width/2, y: CGFloat.random(min:size.height-size.height, max: size.height/2))
            addChild(enemy)
        
        enemy.zRotation = 2*π
        let fullRotation = SKAction.rotate(byAngle: 2*π, duration: 7.0)
        
        let actionTranslation = SKAction.move(to:CGPoint(x: -enemy.size.width/2,y: enemy.position.y), duration: 2.0)
        let group = SKAction.group([fullRotation,actionTranslation])
        
        let actionRemove = SKAction.removeFromParent()
        enemy.run(SKAction.sequence([group, actionRemove]))
    }
    
    func spawnEnemyDistinto() {
        let enemy = SKSpriteNode(imageNamed: "enemyasteroid.png")
        enemy.name = "enemyasteroid"
        enemy.zPosition = 1
        enemy.position = CGPoint(x: size.width + enemy.size.width/2, y: CGFloat.random(min:size.height-size.height, max: size.height/2))
       
        addChild(enemy)
        
        enemy.zRotation = 2*π
        let fullRotation = SKAction.rotate(byAngle: 2*π, duration: 5.0)
        
        let actionTranslation = SKAction.move(to:CGPoint(x: -enemy.size.width/2,y: enemy.position.y), duration: 2.0)
        
        let group = SKAction.group([fullRotation,actionTranslation])
        
        let actionRemove = SKAction.removeFromParent()
        enemy.run(SKAction.sequence([group, actionRemove]))
    }

    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first! as UITouch
        let location = touch.location(in: self)
        let node = self.atPoint(location)
        
        
        if node.name == "playbutton" {
            
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            let moveChimp = SKAction.moveTo(x: self.size.width + self.chimp.size.width, duration: 1.0)
            
            
            chimp.run(moveChimp, completion: {
                let levelScene = LevelScene(size: self.size)
                levelScene.scaleMode = self.scaleMode
                let transition = SKTransition.push(with: .up, duration: 1.0)
                self.view?.presentScene(levelScene,transition: transition)
                
            })
            
           
        }
        
     
    }
    
    override func update(_ currentTime: TimeInterval) {
        
        animateChimp()
        
        
        
    }
    
}
