//
//  LevelScene.swift
//  SpaceChimpy
//
//  Created by Cesar Castillo on 14/03/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import Foundation
import SpriteKit

class LevelScene: SKScene {
    
    var easyButton = SKSpriteNode(imageNamed: "easybutton")
    var normalButton = SKSpriteNode(imageNamed: "normalbutton")
    var hardButton = SKSpriteNode(imageNamed: "hardbutton")
    var arcadeButton = SKSpriteNode(imageNamed: "arcadebutton")

    
    override init (size: CGSize) {
        
        
        super.init(size: size)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("El metodo init no ha sido implementado")
    }
    

    
    override  func didMove(to view: SKView) {
        let background = SKSpriteNode(imageNamed: "backgroundlevels")
        background.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        background.zPosition = -1
        addChild(background)
        
        easyButton.position = CGPoint(x: size.width/2, y: size.height/2+380)
        easyButton.zPosition = 1
        easyButton.xScale = 1.5
        easyButton.yScale = 1.5
        easyButton.name = "easybutton"
        addChild(easyButton)
        
        normalButton.position = CGPoint(x: size.width/2, y: size.height/2+125)
        normalButton.zPosition = 1
        normalButton.xScale = 1.5
        normalButton.yScale = 1.5
        normalButton.name = "normalbutton"
        addChild(normalButton)
        
        hardButton.position = CGPoint(x: size.width/2, y: size.height/2-125)
        hardButton.zPosition = 1
        hardButton.xScale = 1.5
        hardButton.yScale = 1.5
        hardButton.name = "hardbutton"
        addChild(hardButton)
        
        arcadeButton.position = CGPoint(x: size.width/2, y: size.height/4)
        arcadeButton.zPosition = 1
        arcadeButton.xScale = 1.5
        arcadeButton.yScale = 1.5
        arcadeButton.name = "arcadebutton"
        addChild(arcadeButton)
        
        //backgroundAudioPlayer.stop()
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let touch = touches.first! as UITouch
        let location = touch.location(in: self)
        let node = self.atPoint(location)
        
        
        if node.name == "easybutton" {
            
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            
               easyButton.run(fullScale, completion: {
                let gameScene = GameScene(size: self.size,tipo:"easy")
                gameScene.scaleMode = self.scaleMode
                let transition = SKTransition.push(with: .down, duration: 1.0)
                self.view?.presentScene(gameScene, transition: transition)

                
            })

            
            
            
        } else if node.name == "normalbutton" {
            
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            
            normalButton.run(fullScale, completion: {
            
            let gameScene = GameScene(size: self.size,tipo:"normal")
            gameScene.scaleMode = self.scaleMode
            let transition = SKTransition.push(with: .down, duration: 1.0)
            self.view?.presentScene(gameScene, transition: transition)
            
             })
            
        } else if node.name == "hardbutton" {
            
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            
            hardButton.run(fullScale, completion: {
            
            let gameScene = GameScene(size: self.size,tipo:"hard")
            gameScene.scaleMode = self.scaleMode
            let transition = SKTransition.push(with: .down, duration: 1.0)
            self.view?.presentScene(gameScene, transition: transition)
            })

        } else if node.name == "arcadebutton" {
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.25)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            
            arcadeButton.run(fullScale, completion: {
                
                let gameScene = GameScene(size: self.size,tipo:"arcade")
                gameScene.scaleMode = self.scaleMode
                let transition = SKTransition.push(with: .down, duration: 1.0)
                self.view?.presentScene(gameScene, transition: transition)
            })
            
        }
        
    }
}
