//
//  GameScene.swift
//  SpaceChimpy
//
//  Created by Cesar Castillo on 17/03/17.
//  Copyright © 2017 Cesar Castillo. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var chimp = SKSpriteNode(imageNamed: "chimp1")
    let chimpAnimation : SKAction
    var lastUpdatedTime : TimeInterval = 0
    var dt : TimeInterval = 0
    var chimpPixelsPerSecond : CGFloat = 300.0
    let chimpAnglesPerSecond : CGFloat = 4.0 * π
    var velocity = CGPoint.zero
    var playableArea : CGRect?
    var lastTouchLocation = CGPoint.zero
    var isInvisible : Bool
    let bananaPixelPerSecond : CGFloat = 650.0
    var chimpLives = 3
    var congaCount = 0
    var objetive = 25
    var isGameOver = false
    var tipo : String
    var highscore = 0
    
    
    let backgroundPixelPerSecond : CGFloat = 150.0
    
    let backgroundLayer = SKNode()
    let pauseButton = SKSpriteNode(imageNamed: "pausebutton")
    let livesLabel : SKLabelNode
    let bananaLabel : SKLabelNode
    let livesHeart = SKSpriteNode(imageNamed: "heart")
    let banana = SKSpriteNode(imageNamed: "banana")
    let highLabel : SKLabelNode
    let bananaSound = SKAction.playSoundFileNamed("banana", waitForCompletion: false)
    let enemySound = SKAction.playSoundFileNamed("monkey", waitForCompletion: false)
    let tapToResume = SKLabelNode(fontNamed: "Noteworthy")
    var pauseNodeBackground = SKSpriteNode(imageNamed: "pausebackground")
    var resumeButton = SKSpriteNode(imageNamed: "resume")
    var menuButton = SKSpriteNode(imageNamed: "menu")
    var highscorePause = SKSpriteNode(imageNamed: "highscore")
    var gameOverSprite = SKSpriteNode(imageNamed: "gameoverbutton")
    var youWinSprite = SKSpriteNode(imageNamed: "youwinbutton")
    
    
    
    
    init (size: CGSize,tipo: String) {
        
        self.tipo = tipo
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            let maxAspectRatio : CGFloat = 16.0/9.0
            let playableHeight = size.width / maxAspectRatio
            let playableMargin = (size.height - playableHeight) / 2.0
            
        
        playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
        } else if UIDevice.current.userInterfaceIdiom == .pad {
            
            let maxAspectRatio : CGFloat = 4.0/3.0
            let playableHeight = size.width / maxAspectRatio
            let playableMargin = (size.height - playableHeight) / 2.0
            
            playableArea = CGRect(x: 0, y: playableMargin, width: size.width, height: playableHeight)
            
        }
        
        var chimpTextures : [SKTexture] = []
        
        for i in 1...4 {
            chimpTextures.append(SKTexture(imageNamed: "chimp\(i)"))
        }
        
        chimpTextures.append(chimpTextures[2])
        chimpTextures.append(chimpTextures[1])
        
        chimpAnimation = SKAction.repeatForever(SKAction.animate(with: chimpTextures, timePerFrame: 0.15))
        isInvisible = false
        
        livesLabel = SKLabelNode(fontNamed: "AppleSDGothicNeo-SemiBold")
        bananaLabel = SKLabelNode(fontNamed: "AppleSDGothicNeo-SemiBold")
        highLabel = SKLabelNode(fontNamed: "AppleSDGothicNeo-SemiBold")
        
        
       
        super.init(size: size)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("El metodo init no ha sido implementado")
    }
    
    override func didMove(to view: SKView) {
        
        
        
        
        pauseButton.position = CGPoint(x: size.width/2, y: (playableArea?.maxY)!-90)
        pauseButton.zPosition = 1
        pauseButton.xScale = 0.75
        pauseButton.yScale = 0.75
        pauseButton.name = "pausebutton"
        pauseButton.isHidden = false
        addChild(pauseButton)
        

        NotificationCenter.default.addObserver(self, selector:#selector(pauseGame), name: NSNotification.Name(rawValue: "pauseGame"), object: nil)

        
        if self.tipo == "easy" {
            chimpPixelsPerSecond = 350.0
            chimpLives = 5
            objetive = 25
            
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnHeart),SKAction.wait(forDuration: 500.0)])))
            
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemy),SKAction.wait(forDuration: 3.0)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnBanana),SKAction.wait(forDuration: 3.0)])))
            
            let hightscoreDefaults = UserDefaults.standard
            if (hightscoreDefaults.value(forKey: "Highscoreeasy") != nil) {
                highscore = hightscoreDefaults.value(forKey: "Highscoreeasy") as! NSInteger
            }
            
        } else if self.tipo == "normal" {
            
            chimpPixelsPerSecond = 350.0
            chimpLives = 3
            objetive = 50
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnHeart),SKAction.wait(forDuration: 100.0)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemy),SKAction.wait(forDuration: 1.5)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyDistinto),SKAction.wait(forDuration: 15.0)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnBanana),SKAction.wait(forDuration: 5.0)])))
            
            let hightscoreDefaults = UserDefaults.standard
            if (hightscoreDefaults.value(forKey: "Highscorenormal") != nil) {
                highscore = hightscoreDefaults.value(forKey: "Highscorenormal") as! NSInteger
            }
            
        } else if self.tipo == "hard" {
            chimpPixelsPerSecond = 350.0
            chimpLives = 2
            objetive = 100
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnHeart),SKAction.wait(forDuration: 50.0)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemy),SKAction.wait(forDuration: 1.0)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyDistinto),SKAction.wait(forDuration: 2.0)])))
            
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnBanana),SKAction.wait(forDuration: 5.0)])))
            
            let hightscoreDefaults = UserDefaults.standard
            if (hightscoreDefaults.value(forKey: "Highscorehard") != nil) {
                highscore = hightscoreDefaults.value(forKey: "Highscorehard") as! NSInteger
            }
            
        } else if self.tipo == "arcade" {
            
            chimpPixelsPerSecond = 350.0
            chimpLives = 1
            objetive = 10000000
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnHeart),SKAction.wait(forDuration: 50.0)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemy),SKAction.wait(forDuration: 1.5)])))
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyDistinto),SKAction.wait(forDuration: 3.0)])))
            
            
            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnBanana),SKAction.wait(forDuration: 2.0)])))
            
              run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnNave),SKAction.wait(forDuration: 20.0)])))
            
            
            let hightscoreDefaults = UserDefaults.standard
            if (hightscoreDefaults.value(forKey: "Highscorearcade") != nil) {
                highscore = hightscoreDefaults.value(forKey: "Highscorearcade") as! NSInteger
            }

            
        }
        
        backgroundLayer.zPosition = -1
        addChild(backgroundLayer)
        for i in 0...1 {
            let background = backgroundNode()
            background.position = CGPoint(x: CGFloat(i) * background.size.width, y: 0.0)
            background.anchorPoint = CGPoint.zero
            background.zPosition = -1
            background.name = "background"
            backgroundLayer.addChild(background)
        }
        
        
        
        chimp.position = CGPoint(x: 600, y: 600)
        chimp.zPosition = 1
        
        backgroundLayer.addChild(chimp)
    
        playBackgroundMusic(filename: "backgroundMusic.mp3")
        
        
        livesHeart.position = CGPoint(x: 200.0, y: (playableArea?.maxY)!-90)
        livesHeart.xScale = 3/4
        livesHeart.yScale = 3/4
        addChild(livesHeart)
        
        banana.position = CGPoint(x:(playableArea?.maxX)!-100.0, y: (playableArea?.maxY)!-90)
        addChild(banana)
        
        
        livesLabel.position = CGPoint(x: 50.0, y: (playableArea?.maxY)!-50)
        livesLabel.fontSize = 100
        livesLabel.verticalAlignmentMode = .top
        livesLabel.horizontalAlignmentMode = .left
        addChild(livesLabel)
        
        
        
        bananaLabel.position = CGPoint(x:(playableArea?.maxX)!-200.0, y: (playableArea?.maxY)!-50)
        bananaLabel.fontSize = 100
        bananaLabel.verticalAlignmentMode = .top
        bananaLabel.horizontalAlignmentMode = .right
        addChild(bananaLabel)
        
        animateChimp()
        updateHUD()
        
        
    }
    

    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      
        let touch = touches.first! as UITouch
        let location = touch.location(in: backgroundLayer)
        sceneTouched(touchLocation: location)
        
        
                let touch2 = touches.first! as UITouch
                let location2 = touch2.location(in: self)
                let node = self.atPoint(location2)
        
       
                if node.name == "pausebutton" {

                    if (self.speed != 0) {
                        
                       pauseGame()
                    }
                    
                }else if node.name == "menu" {
                    
                    
                  
                        //print("toque el boton")
                        let mainMenu = MainMenuScene(size: self.size)
                        mainMenu.scaleMode = self.scaleMode
                        let transition = SKTransition.reveal(with: .up, duration: 1.0)
                        self.view?.presentScene(mainMenu, transition: transition)
                  
                    
                        
                    }else if node.name == "resume" {

                        
                        if tipo == "easy" {
                            isInvisible = false
                            self.speed = 1.0
                            chimp.speed = 1.0
                            self.pauseButton.isHidden = false
                            backgroundAudioPlayer.play()
                            chimpPixelsPerSecond = 350.0
                            pauseNodeBackground.removeFromParent()
                            resumeButton.removeFromParent()
                            menuButton.removeFromParent()
                            highscorePause.removeFromParent()
                            highLabel.removeFromParent()
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnHeart),SKAction.wait(forDuration: 500.0)])))
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemy),SKAction.wait(forDuration: 3.0)])))
                            
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnBanana),SKAction.wait(forDuration: 3.0)])))
                            
                        
                        }else if tipo == "normal" {
                            
                            isInvisible = false
                            self.speed = 1.0
                            chimp.speed = 1.0
                            self.pauseButton.isHidden = false
                            backgroundAudioPlayer.play()
                            chimpPixelsPerSecond = 350.0
                            pauseNodeBackground.removeFromParent()
                            resumeButton.removeFromParent()
                            menuButton.removeFromParent()
                            highscorePause.removeFromParent()
                            pauseNodeBackground.removeFromParent()
                             highLabel.removeFromParent()
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnHeart),SKAction.wait(forDuration: 100.0)])))
                            
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemy),SKAction.wait(forDuration: 1.5)])))
                          run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyDistinto),SKAction.wait(forDuration: 15.0)])))
                            
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnBanana),SKAction.wait(forDuration: 5.0)])))
                            
                            

                            
                        } else if tipo == "hard" {
                            isInvisible = false
                            self.speed = 1.0
                            chimp.speed = 1.0
                            self.pauseButton.isHidden = false
                            backgroundAudioPlayer.play()
                            chimpPixelsPerSecond = 350.0
                            pauseNodeBackground.removeFromParent()
                            resumeButton.removeFromParent()
                            menuButton.removeFromParent()
                            highscorePause.removeFromParent()
                            highLabel.removeFromParent()
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnHeart),SKAction.wait(forDuration: 50.0)])))
                            
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemy),SKAction.wait(forDuration: 1.0)])))
                            
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyDistinto),SKAction.wait(forDuration: 2.0)])))
                            
                            
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnBanana),SKAction.wait(forDuration: 5.0)])))
                            

                        } else if tipo == "arcade" {
                            isInvisible = false
                            self.speed = 1.0
                            chimp.speed = 1.0
                            self.pauseButton.isHidden = false
                            backgroundAudioPlayer.play()
                            chimpPixelsPerSecond = 350.0
                            pauseNodeBackground.removeFromParent()
                            resumeButton.removeFromParent()
                            menuButton.removeFromParent()
                            highscorePause.removeFromParent()
                            pauseNodeBackground.removeFromParent()
                            highLabel.removeFromParent()
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnHeart),SKAction.wait(forDuration: 50.0)])))
                            
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemy),SKAction.wait(forDuration: 1.5)])))
                            
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnEnemyDistinto),SKAction.wait(forDuration: 8.0)])))
                            
                            
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnBanana),SKAction.wait(forDuration: 2.0)])))
                            
                            run(SKAction.repeatForever(SKAction.sequence([SKAction.run(spawnNave),SKAction.wait(forDuration: 20.0)])))
                            
                    }
            }
                    
    }
    



    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first! as UITouch
        let location = touch.location(in: backgroundLayer)
        sceneTouched(touchLocation: location)
       
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        
        moveSprite(sprite: chimp, velocity: velocity)
        rotateSprite(sprite: chimp, direction: velocity)
        
        
        if lastUpdatedTime > 0 {
            dt = currentTime - lastUpdatedTime
        } else  {
            dt = 0
        }
        
        lastUpdatedTime = currentTime
        moveSprite(sprite: chimp, velocity: velocity)
        checkBounds()
        rotateSprite(sprite: chimp, direction: velocity)
        //moveConga()
        if self.speed != 0 {
        moveBackground()
        }
        if chimpLives <= 0 && !isGameOver {
            isGameOver = true
            print("el juego ha acabado")
            backgroundAudioPlayer.stop()
            isInvisible = true
            
            if congaCount > highscore {
                highscore = congaCount
                
                
                if tipo == "easy" {
                
                let hightscoreDefaults = UserDefaults.standard
                hightscoreDefaults.set(highscore, forKey: "Highscoreeasy")
                hightscoreDefaults.synchronize()
                
                print(highscore)
                } else if tipo == "normal" {
                 
                    let hightscoreDefaults = UserDefaults.standard
                    hightscoreDefaults.set(highscore, forKey: "Highscorenormal")
                    hightscoreDefaults.synchronize()
                    
                } else if tipo == "hard" {
                    
                    let hightscoreDefaults = UserDefaults.standard
                    hightscoreDefaults.set(highscore, forKey: "Highscorehard")
                    hightscoreDefaults.synchronize()
                    
                } else if tipo == "arcade" {
                    
                    let hightscoreDefaults = UserDefaults.standard
                    hightscoreDefaults.set(highscore, forKey: "Highscorearcade")
                    hightscoreDefaults.synchronize()
                    
                }
                
            }
            
            
            self.gameOverSprite.position = CGPoint(x: size.width/2, y: size.height/2)
            self.gameOverSprite.xScale = 0.8
            self.gameOverSprite.yScale = 0.8
            self.gameOverSprite.zPosition = 3
            addChild(gameOverSprite)
            
           
            run(SKAction.sequence([SKAction.wait(forDuration: 0.25), SKAction.playSoundFileNamed("lose", waitForCompletion: false)]))
            //run(SKAction.playSoundFileNamed("lose", waitForCompletion: false))
            
            pauseButton.isHidden = true
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.8)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            let repeatAction = SKAction.repeat(fullScale, count: 2)
            
            
            gameOverSprite.run(repeatAction, completion: {
                
                self.isInvisible = true
                self.chimpPixelsPerSecond = 0.0
                self.velocity = CGPoint.zero
                self.speed = 0.0
                self.chimp.speed = 0.0
                self.removeAllActions()
                self.gameOverSprite.removeFromParent()
                
                self.pauseNodeBackground.position = CGPoint(x:  self.frame.width/2, y: self.frame.height/2)
                self.pauseNodeBackground.size = CGSize(width: self.frame.width/3, height: self.frame.height-500)
                self.pauseNodeBackground.zPosition = 3
                self.addChild(self.pauseNodeBackground)
                
                self.highscorePause.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+300)
                self.highscorePause.zPosition = 4
                self.highscorePause.xScale = 1.5
                self.highscorePause.yScale = 1.5
                self.addChild(self.highscorePause)
                
                
                self.menuButton.name = "menu"
                self.menuButton.zPosition = 4
                self.menuButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-330)
                self.addChild(self.menuButton)
                
                
                self.highLabel.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-150)
                self.highLabel.fontSize = 300
                self.highLabel.text = "\(self.highscore)"
                self.highLabel.zPosition = 4
                self.highLabel.fontColor = #colorLiteral(red: 1, green: 0.8078431373, blue: 0, alpha: 1)
                self.addChild(self.highLabel)

            })
            
            
        }
        
        if congaCount >= objetive && !isGameOver {
            isGameOver = true
            backgroundAudioPlayer.stop()
            isInvisible = true
            
            
            if congaCount > highscore {
                highscore = congaCount
                
                if tipo == "easy" {
                    
                    let hightscoreDefaults = UserDefaults.standard
                    hightscoreDefaults.set(highscore, forKey: "Highscoreeasy")
                    hightscoreDefaults.synchronize()
                    
                    print(highscore)
                } else if tipo == "normal" {
                    
                    let hightscoreDefaults = UserDefaults.standard
                    hightscoreDefaults.set(highscore, forKey: "Highscorenormal")
                    hightscoreDefaults.synchronize()
                    
                } else if tipo == "hard" {
                    
                    let hightscoreDefaults = UserDefaults.standard
                    hightscoreDefaults.set(highscore, forKey: "Highscorehard")
                    hightscoreDefaults.synchronize()
                    
                } else if tipo == "arcade" {
                    
                    let hightscoreDefaults = UserDefaults.standard
                    hightscoreDefaults.set(highscore, forKey: "Highscorearcade")
                    hightscoreDefaults.synchronize()
                    
                }

                
            }
            
            self.youWinSprite.position = CGPoint(x: size.width/2, y: size.height/2)
            self.youWinSprite.xScale = 0.8
            self.youWinSprite.yScale = 0.8
            self.youWinSprite.zPosition = 3
            addChild(youWinSprite)
            
            run(SKAction.sequence([SKAction.wait(forDuration: 0.25), SKAction.playSoundFileNamed("win", waitForCompletion: false)]))
            //run(SKAction.playSoundFileNamed("lose", waitForCompletion: false))
            
            
            let scaleUP = SKAction.scale(by: 1.25, duration: 0.8)
            let scaleDown = scaleUP.reversed()
            let fullScale = SKAction.sequence([scaleUP,scaleDown])
            
            let repeatAction = SKAction.repeat(fullScale, count: 2)
            
            
            youWinSprite.run(repeatAction, completion: {
               
                self.isInvisible = true
                self.chimpPixelsPerSecond = 0.0
                self.velocity = CGPoint.zero
                self.speed = 0.0
                self.chimp.speed = 0.0
                self.removeAllActions()
                self.youWinSprite.removeFromParent()
                
                self.pauseNodeBackground.position = CGPoint(x:  self.frame.width/2, y: self.frame.height/2)
                self.pauseNodeBackground.size = CGSize(width: self.frame.width/3, height: self.frame.height-500)
                self.pauseNodeBackground.zPosition = 3
                self.addChild(self.pauseNodeBackground)
                
                self.highscorePause.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+300)
                self.highscorePause.zPosition = 4
                self.highscorePause.xScale = 1.5
                self.highscorePause.yScale = 1.5
                self.addChild(self.highscorePause)
                
                
                self.menuButton.name = "menu"
                self.menuButton.zPosition = 4
                self.menuButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-330)
                self.addChild(self.menuButton)
                
                
                self.highLabel.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-150)
                self.highLabel.fontSize = 300
                self.highLabel.text = "\(self.highscore)"
                self.highLabel.zPosition = 4
                self.highLabel.fontColor = #colorLiteral(red: 1, green: 0.8078431373, blue: 0, alpha: 1)
                self.addChild(self.highLabel)

               
            
            })
            
        }
        
        
    }
    
    func pauseGame() {
        
        self.run(SKAction.playSoundFileNamed("pause", waitForCompletion: false), completion: {
            
            self.pauseNodeBackground.position = CGPoint(x:  self.frame.width/2, y: self.frame.height/2)
            self.pauseNodeBackground.size = CGSize(width: self.frame.width/3, height: self.frame.height-500)
            //pauseNodeBackground.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            self.pauseNodeBackground.zPosition = 3
            
            self.menuButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-180)//(x: self.pauseNodeBackground.size.width/60, y: -self.pauseNodeBackground.size.height/3)
            self.menuButton.name = "menu"
            self.menuButton.zPosition = 4
            
            self.resumeButton.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-350)//(x: self.pauseNodeBackground.size.width/60, y: -self.pauseNodeBackground.size.height/6)
            self.resumeButton.name = "resume"
            self.resumeButton.zPosition = 4
            
            self.highscorePause.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2+300)
            self.highscorePause.zPosition = 4
            self.highscorePause.xScale = 1.5
            self.highscorePause.yScale = 1.5
            
            
            
            self.addChild(self.pauseNodeBackground)
            self.addChild(self.resumeButton)
            self.addChild(self.menuButton)
            self.addChild(self.highscorePause)
            self.highLabel.position = CGPoint(x: self.frame.width/2, y: self.frame.height/2-85)
            self.highLabel.fontSize = 300
            self.highLabel.text = "\(self.highscore)"
            self.highLabel.zPosition = 4
            self.highLabel.fontColor = #colorLiteral(red: 1, green: 0.8078431373, blue: 0, alpha: 1)
            self.addChild(self.highLabel)
            backgroundAudioPlayer.pause()
            self.isInvisible = true
            self.chimpPixelsPerSecond = 0.0
            self.velocity = CGPoint.zero
            self.speed = 0.0
            self.chimp.speed = 0.0
            self.pauseButton.isHidden = true
            self.removeAllActions()
            
        })

        
    }
    
    
    //Se ejecuta al final del update o despues del update en cada frame
    override func didEvaluateActions() {
        checkCollisions()
    }
    

    
    func moveSprite(sprite:SKSpriteNode, velocity: CGPoint) {
        // espacio = velocidad + tiempo
        
        let amount = velocity * CGFloat(dt)
        
        sprite.position += amount //CGPoint(x: sprite.position.x + amount.x, y: sprite.position.y + amount.y)
        
    }
    
    func moveChimpToLocation(location:CGPoint) {
        
        animateChimp()
        
        let offset = location - chimp.position//CGPoint(x: location.x - zombie.position.x, y: location.y - zombie.position.y)
        //let offsetLength = offset.length()//sqrt(Double(offset.x * offset.x + offset.y * offset.y))
        let direction = offset.normalize()//CGPoint(x: offset.x / CGFloat(offsetLength), y: offset.y / CGFloat(offsetLength))
        velocity = direction * chimpPixelsPerSecond//CGPoint(x: direction.x * zombiePixelsPerSecond, y: direction.y * zombiePixelsPerSecond)
        
    }
    
    func sceneTouched(touchLocation : CGPoint) {
        lastTouchLocation = touchLocation
        moveChimpToLocation(location: touchLocation)
    }
    
    func checkBounds() {
        let bottomLeft = backgroundLayer.convert(CGPoint(x: 0, y: playableArea!.minY), from: self)
        let upperRight = backgroundLayer.convert(CGPoint(x: size.width, y: playableArea!.maxY), from: self)
        
        if chimp.position.x <= bottomLeft.x {
            chimp.position.x = bottomLeft.x
            velocity.x = -velocity.x
        }
        if chimp.position.y <= bottomLeft.y {
            chimp.position.y = bottomLeft.y
            velocity.y = -velocity.y
        }
        if chimp.position.x >= upperRight.x {
            chimp.position.x = upperRight.x
            velocity.x = -velocity.x
        }
        if chimp.position.y >= upperRight.y {
            chimp.position.y = upperRight.y
            velocity.y = -velocity.y
        }
    }
    
    func rotateSprite(sprite:SKSpriteNode, direction: CGPoint) {
        let shortestAngle = shortestAngleBetween(angle1: sprite.zRotation, angle2: velocity.angle())
        let amountToRotate = min(chimpAnglesPerSecond * CGFloat(dt), abs(shortestAngle))
        
        sprite.zRotation += amountToRotate * shortestAngle.sign()//direction.angle()//CGFloat(atan2(Double(direction.y),Double(direction.x)))
    }
    
    //soltar enemigos en pantalla
    func spawnEnemy() {
        var enemy = SKSpriteNode()
        
        if self.tipo == "easy" {
            
            enemy = SKSpriteNode(imageNamed: "enemy.png")
            enemy.name = "enemy"
            
        } else if self.tipo == "normal" {
            
            enemy = SKSpriteNode(imageNamed: "enemyasteroid.png")
            enemy.name = "enemy"
            
            
        } else if self.tipo == "hard" {
            
            enemy = SKSpriteNode(imageNamed: "enemyasteroid.png")
            enemy.name = "enemy"
            
        } else if self.tipo == "arcade" {
            
            enemy = SKSpriteNode(imageNamed: "enemyasteroid.png")
            enemy.name = "enemy"
        }
        
        enemy.position = backgroundLayer.convert(CGPoint(x: size.width + enemy.size.width/2, y: CGFloat.random(min: (playableArea?.minY)! + enemy.size.height/2,max: (playableArea?.maxY)! - enemy.size.height/2)), from: self)
        backgroundLayer.addChild(enemy)
        
        enemy.zRotation = 2*π
        let fullRotation = SKAction.rotate(byAngle: 2*π, duration: 3.0)
        
        let actionTranslation = SKAction.move(to:self.backgroundLayer.convert(CGPoint(x: -enemy.size.width/2,y: enemy.position.y), from: self), duration: 2.5)
        
        
        let group = SKAction.group([fullRotation,actionTranslation])
        let actionRemove = SKAction.removeFromParent()
        enemy.run(SKAction.sequence([group, actionRemove]))
        
    }
    
    
    func spawnEnemyDistinto() {
        var enemy = SKSpriteNode()
        
        if self.tipo == "easy" {
        
            
        } else if self.tipo == "normal" {
            
            enemy = SKSpriteNode(imageNamed: "enemy.png")
            enemy.name = "enemy"
            
            
        } else if self.tipo == "hard" {
            
            enemy = SKSpriteNode(imageNamed: "enemy.png")
            enemy.name = "enemy"
            
        } else if self.tipo == "arcade" {
            
            enemy = SKSpriteNode(imageNamed: "enemy.png")
            enemy.name = "enemy"
            
        }
        
        enemy.position = backgroundLayer.convert(CGPoint(x: size.width + enemy.size.width/2, y: CGFloat.random(min: (playableArea?.minY)! + enemy.size.height/2,max: (playableArea?.maxY)! - enemy.size.height/2)), from: self)
        backgroundLayer.addChild(enemy)
        
        enemy.zRotation = 2*π
        let fullRotation = SKAction.rotate(byAngle: 2*π, duration: 3.0)
        
        let actionTranslation = SKAction.move(to:self.backgroundLayer.convert(CGPoint(x: -enemy.size.width/2,y: enemy.position.y), from: self), duration: 2.5)
        
        
        let group = SKAction.group([fullRotation,actionTranslation])
        let actionRemove = SKAction.removeFromParent()
        enemy.run(SKAction.sequence([group, actionRemove]))
        
    }
    
    
    func spawnNave() {
        var enemy = SKSpriteNode()
    
        if self.tipo == "arcade" {
            
            enemy = SKSpriteNode(imageNamed: "enemynave.png")
            enemy.name = "enemy"
            
        }
        
        enemy.position = backgroundLayer.convert(CGPoint(x: size.width + enemy.size.width/2, y: CGFloat.random(min: (playableArea?.minY)! + enemy.size.height/2,max: (playableArea?.maxY)! - enemy.size.height/2)), from: self)
        backgroundLayer.addChild(enemy)
        
        
        
        let moveUp = SKAction.moveBy(x: 0.0, y: 10, duration: 1.0)
        let moveDown = SKAction.moveBy(x: 0.0, y: -10, duration: 1.0)
        let fullAnimation = SKAction.sequence([moveUp,moveDown])
        
        
        let actionTranslation = SKAction.move(to:self.backgroundLayer.convert(CGPoint(x: -enemy.size.width/2,y: enemy.position.y), from: self), duration: 5.0)
        
        
        let group = SKAction.group([fullAnimation,actionTranslation])
        let actionRemove = SKAction.removeFromParent()
        enemy.run(SKAction.sequence([group, actionRemove]))
        
    }

    
    func spawnHeart() {
        
        let heart = SKSpriteNode(imageNamed: "heart.png")
        heart.name = "heart"
        heart.position = self.backgroundLayer.convert(CGPoint(x: CGFloat.random(min: (playableArea?.minX)!, max: (playableArea?.maxX)!), y: CGFloat.random(min: (playableArea?.minY)!, max: (playableArea?.maxY)!)), from: self)
        heart.setScale(0)
        
        backgroundLayer.addChild(heart)
        
        //Bloque Rotación
        heart.zRotation = π/16.0
        let rotationLeft = SKAction.rotate(byAngle: π/8.0, duration: 0.25)
        let rotationRight = rotationLeft.reversed()
        let fullRotation = SKAction.sequence([rotationLeft,rotationRight])
        let appear = SKAction.scale(to: 1, duration: 1.0)
        //let wait = SKAction.wait(forDuration: 8.0)
        
        //Bloque Escalado
        let scaleUP = SKAction.scale(by: 1.25, duration: 0.25)
        let scaleDown = scaleUP.reversed()
        let fullScale = SKAction.sequence([scaleUP,scaleDown])
        
        //Bloque de espera
        let group = SKAction.group([fullScale,fullRotation])
        let groupWait = SKAction.repeat(group, count: 16)
        
        let disappear = SKAction.scale(to: 0, duration: 1.0)
        let removeHeart = SKAction.removeFromParent()
        
        let sequence = SKAction.sequence([appear,groupWait,disappear,removeHeart])
        heart.run(sequence)
    }
    
    
    
    func spawnBanana() {
        
        let banana = SKSpriteNode(imageNamed: "banana.png")
        banana.name = "banana"
        banana.position = self.backgroundLayer.convert(CGPoint(x: CGFloat.random(min: (playableArea?.minX)!, max: (playableArea?.maxX)!), y: CGFloat.random(min: (playableArea?.minY)!, max: (playableArea?.maxY)!)), from: self)
        banana.setScale(0)
        
        backgroundLayer.addChild(banana)
        
        //Bloque Rotación
        banana.zRotation = π/16.0
        let rotationLeft = SKAction.rotate(byAngle: π/8.0, duration: 0.25)
        let rotationRight = rotationLeft.reversed()
        let fullRotation = SKAction.sequence([rotationLeft,rotationRight])
        let appear = SKAction.scale(to: 1, duration: 1.0)
        //let wait = SKAction.wait(forDuration: 8.0)
        
        //Bloque Escalado
        let scaleUP = SKAction.scale(by: 1.25, duration: 0.25)
        let scaleDown = scaleUP.reversed()
        let fullScale = SKAction.sequence([scaleUP,scaleDown])
        
        //Bloque de espera
        let group = SKAction.group([fullScale,fullRotation])
        let groupWait = SKAction.repeat(group, count: 16)
        
        let disappear = SKAction.scale(to: 0, duration: 1.0)
        let removeBanana = SKAction.removeFromParent()
        
        let sequence = SKAction.sequence([appear,groupWait,disappear,removeBanana])
        banana.run(sequence)
    }
    
    func animateChimp() {
        if chimp.action(forKey: "animation") == nil {
            chimp.run(SKAction.repeatForever(chimpAnimation), withKey: "animation")
        }
        
    }
    
    func stopChimp() {
        chimp.removeAction(forKey: "animation")
        
    }
    
    func chimpHitsBanana(banana: SKSpriteNode) {
        //cat.removeFromParent()
        if isInvisible == false {
        congaCount += 1
        updateHUD()
        run(bananaSound)
        banana.name = "conga"
        banana.removeAllActions()
        banana.setScale(1.0)
        banana.zRotation = 0.0
        banana.removeFromParent()
        
        }
        //let greenBanana = SKAction.colorize(with: SKColor.green, colorBlendFactor: 1.0, duration: 0.5)
       //banana.run(greenBanana)
        
        
    }
    
    
    func chimpHitsHeart(heart: SKSpriteNode) {
        chimpLives += 1
        updateHUD()
        run(bananaSound)
        heart.name = "heart"
        heart.removeAllActions()
        heart.removeFromParent()
        
       
    }
    
    
    func chimpHitsEnemy(enemy: SKSpriteNode) {
        run(enemySound)
        //loseBananas()
        //if congaCount > 0 {
        //congaCount -= 3
        //}
        chimpLives -= 1
        updateHUD()
        isInvisible = true
        let blinkTimes = 12.0
        let blinkDuration = 4.0
        let blinkAction = SKAction.customAction(withDuration: blinkDuration) { (node, elapsedTime) in
            let slice = blinkDuration/blinkTimes
            let remainder = Double(elapsedTime).truncatingRemainder(dividingBy: slice)
            node.isHidden = remainder > slice/2
            
        }
        
        let setHidden = SKAction.run {
            self.chimp.isHidden = false
            self.isInvisible = false
        }
        
        chimp.run(SKAction.sequence([blinkAction,setHidden]))
        
    }
    
    func checkCollisions() {
        //Comprobar colision con bananas
        
        var hitBananas : [SKSpriteNode] = []
        backgroundLayer.enumerateChildNodes(withName: "banana") { (node, _ in) in
            let banana = node as! SKSpriteNode
            let distance = distanceBetweenPoints(p1: self.chimp.position, p2: banana.position)
            let chimpSize = min(self.chimp.frame.size.width, self.chimp.frame.height)/2.0
            let bananaSize = max(banana.frame.size.width, banana.frame.size.height)/2.0
            if distance < chimpSize + bananaSize {
                hitBananas.append(banana)
            }
        }
        
        for banana in hitBananas {
            chimpHitsBanana(banana: banana)
        }
        
        
        if self.isInvisible {
            return
        }
        //Comprobar colision con enemigos
        
        var hitEnemies : [SKSpriteNode] = []
        backgroundLayer.enumerateChildNodes(withName: "enemy") { (node, _ in) in
            let enemy = node as! SKSpriteNode
            let distance = distanceBetweenPoints(p1: self.chimp.position, p2: enemy.position)
            let chimpSize = max(self.chimp.frame.size.width, self.chimp.frame.height)/3.0
            let enemySize = max(enemy.frame.size.width, enemy.frame.height)/3.0
            if distance < chimpSize + enemySize {
                hitEnemies.append(enemy)
            }
        }
        
        for enemy in hitEnemies {
            chimpHitsEnemy(enemy: enemy)
        }
        
        var hitHearts : [SKSpriteNode] = []
        backgroundLayer.enumerateChildNodes(withName: "heart") { (node, _ in) in
            let heart = node as! SKSpriteNode
            let distance = distanceBetweenPoints(p1: self.chimp.position, p2: heart.position)
            let chimpSize = max(self.chimp.frame.size.width, self.chimp.frame.height)/2.0
            let heartSize = max(heart.frame.size.width, heart.frame.height)/2.0
            if distance < chimpSize + heartSize {
                hitHearts.append(heart)
            }
        }
        
        for heart in hitHearts {
            chimpHitsHeart(heart: heart)
        }
        
        
        
        
        
    }
    
    func moveConga() {
        var congaPosition = chimp.position
        
        backgroundLayer.enumerateChildNodes(withName: "conga") { (node, _ in) in
            if !node.hasActions() {
                let actionDuration = 0.2
                let offset = congaPosition - node.position
                let director = offset.normalize()
                let amountToMovePerSecond = director * self.bananaPixelPerSecond
                let totalAmountToMove = amountToMovePerSecond * CGFloat(actionDuration)
                
                let actionMove = SKAction.moveBy(x: totalAmountToMove.x, y: totalAmountToMove.y, duration: actionDuration)
                let actionRotate = SKAction.rotate(byAngle: offset.angle() - node.zRotation, duration: actionDuration)
                
                node.run(SKAction.group([actionMove,actionRotate]))
            }
            congaPosition = node.position
        }
    }
    
    func loseBananas() {
        var lostBananasCount = 0
        backgroundLayer.enumerateChildNodes(withName: "conga") { node,stop in
           var randomBananaPosition = node.position
            randomBananaPosition.x += CGFloat.random(min: -150, max: 150)
            randomBananaPosition.y += CGFloat.random(min: -150, max: 150)
            node.name = ""
            
            let rotation = SKAction.rotate(byAngle: 6*π, duration: 1.0)
            let translate = SKAction.move(to: randomBananaPosition, duration: 1.0)
            let disappear = SKAction.scale(to: 0, duration: 1.0)
            
            let groupAction = SKAction.group([rotation,translate,disappear])
            let fullSequence = SKAction.sequence([groupAction, SKAction.removeFromParent()])
            node.run(fullSequence)
            
            lostBananasCount += 1
            self.congaCount -= 1
            self.updateHUD()
            
            if lostBananasCount >= 3 {
                stop.initialize(to: true)
            }
        }
    }
    
    func backgroundNode() -> SKSpriteNode {
        
        
        let background = SKSpriteNode()
        background.anchorPoint = CGPoint.zero
        background.name = "background"
        
        let background1 = SKSpriteNode(imageNamed: "background1")
        background1.anchorPoint = CGPoint.zero
        background1.position = CGPoint.zero
        background.addChild(background1)
        
        let background2 = SKSpriteNode(imageNamed: "background2")
        background2.anchorPoint = CGPoint.zero
        background2.position = CGPoint(x: background1.size.width, y: 0.0)
        background.addChild(background2)
        
        background.size = CGSize(width: background1.size.width + background2.size.width, height: background1.size.height)
        
        return background
        
    }
    
    func moveBackground() {
        
        let velocity = CGPoint(x:-self.backgroundPixelPerSecond, y: 0.0)
        let amountToMove = velocity * CGFloat(self.dt)
        backgroundLayer.position += amountToMove
        
        backgroundLayer.enumerateChildNodes(withName: "background") { (node, _) in
            let background = node as! SKSpriteNode
            
            let backgroundLayerposition = self.backgroundLayer.convert(background.position, to: self)
            
            if backgroundLayerposition.x <= -background.size.width {
                background.position = CGPoint(x: background.position.x + 2.0*background.size.width, y: 0.0)
            }
        }
    }
    
    func updateHUD() {
        if isInvisible == false || isGameOver == true {
        livesLabel.text = "\(chimpLives)"
        bananaLabel.text = "\(congaCount)/\(objetive)"
        if tipo == "arcade" {
        bananaLabel.text = "\(congaCount) "
        }
    }
  }
}

